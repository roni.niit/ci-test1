const express = require("express");
const port = 3000;
const app = express();

app.get("/", (req, res) => res.send("Welcome"));

// multiplication
app.get("/multi", (req, res) => {
    try {
        console.log(
            {
            a: parseInt(req.query.a),
            b: parseInt(req.query.b)
            }
        );
        const multi = parseInt(req.query.a) * parseInt(req.query.b);
        res.send(multi.toString());
    } catch (e) {
        res.sendStatus(500); // Server internal error
    }
});

if (process.env.NODE_ENV === "test") {
    module.exports = app;
}

if(!module.parent){
app.listen(port, () => console.log(`Server running at localhost:${port}`));
}